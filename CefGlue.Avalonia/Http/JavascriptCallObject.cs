﻿using System.Collections.Generic;

namespace CefGlue.Avalonia.Http
{
    public class JavascriptCallObject
    {
        public int ObjectId { get; set; }
        public string ObjectType { get; set; }

        public string MethodName { get; set; }

        public string AssemblyName { get; set; }

        public Dictionary<int, object> CallParams { get; set; }
    }

    public class JavsscriptResultObject
    {
        public bool IsSuccess { get; set; } = false;

        public string Message { get; set; }

        public object Object { get; set; }
    }
}
