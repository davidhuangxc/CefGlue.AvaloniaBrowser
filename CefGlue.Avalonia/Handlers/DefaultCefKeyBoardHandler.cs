﻿using CefGlue.Avalonia.CefExInterop;
using System;
using Xilium.CefGlue;

namespace CefGlue.Avalonia.Handlers
{
    internal class DefaultCefKeyBoardHandler : CefKeyboardHandler
    {
        private AvaloniaCefBrowser Owner;

        public DefaultCefKeyBoardHandler(AvaloniaCefBrowser owner)
        {
            Owner = owner;
        }

        protected override bool OnPreKeyEvent(CefBrowser browser, CefKeyEvent keyEvent, IntPtr os_event, out bool isKeyboardShortcut)
        {
            isKeyboardShortcut = false;
            bool handled = false;
            if (keyEvent.EventType == CefKeyEventType.RawKeyDown)
            {
                switch (keyEvent.WindowsKeyCode)
                {
                    ///F12
                    case 0x7B:
                        if (CefRuntime.Platform == CefRuntimePlatform.Windows)
                        {
                            var cefwindow = CefWindowInfo.Create();
                            cefwindow.SetAsPopup(IntPtr.Zero, "Dev Tool");
                            browser.GetHost().ShowDevTools(cefwindow, new CefKeyBoardClient(), new CefBrowserSettings(), new CefPoint(0, 0));
                        }
                        else if (CefRuntime.Platform == CefRuntimePlatform.Linux)
                        {
                            ShowLinuxDebugWindow.ShowDebugWindow(CommonConst.CefDebugPort);
                        }
                        handled = true;
                        break;
                    // F5
                    case 0x74:
                        browser.ReloadIgnoreCache();
                        handled = true;
                        break;
                    case 0x08:
                        browser.GoBack();
                        break;
                    case 0x22:
                        browser.GoForward();
                        break;
                }
            }

            return handled;
        }
        protected override bool OnKeyEvent(CefBrowser browser, CefKeyEvent keyEvent, IntPtr osEvent)
        {
            return base.OnKeyEvent(browser, keyEvent, osEvent);
        }
    }
}
