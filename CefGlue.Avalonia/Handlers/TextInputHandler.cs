﻿using Avalonia;
using Avalonia.Controls.Presenters;
using Avalonia.Input.TextInput;
using System;

namespace CefGlue.Avalonia.Handlers
{
    internal class MyTextBoxTextInputMethodClient : TextInputMethodClient
    {
        private TextPresenter _presenter;
        private IDisposable _subscription;
        public override Rect CursorRectangle => new Rect(new Point(x, y), new Point(x, y + 5)); //_presenter?.GetCursorRectangle() ?? default;
#pragma warning disable CS0108 // 成员隐藏继承的成员；缺少关键字 new
        public event EventHandler CursorRectangleChanged;
#pragma warning restore CS0108 // 成员隐藏继承的成员；缺少关键字 new
        public override Visual TextViewVisual => _presenter;
#pragma warning disable CS0108 // 成员隐藏继承的成员；缺少关键字 new
        public event EventHandler TextViewVisualChanged;
#pragma warning restore CS0108 // 成员隐藏继承的成员；缺少关键字 new
        public override bool SupportsPreedit => false;
#pragma warning disable CS0114 // 成员隐藏继承的成员；缺少关键字 override
        public void SetPreeditText(string text) => throw new NotSupportedException();
#pragma warning restore CS0114 // 成员隐藏继承的成员；缺少关键字 override

        public override bool SupportsSurroundingText => false;
        public override string SurroundingText =>"";
#pragma warning disable CS0108 // 成员隐藏继承的成员；缺少关键字 new
#pragma warning disable CS0067 // 从不使用事件“MyTextBoxTextInputMethodClient.SurroundingTextChanged”
        public event EventHandler SurroundingTextChanged;
#pragma warning restore CS0067 // 从不使用事件“MyTextBoxTextInputMethodClient.SurroundingTextChanged”
#pragma warning restore CS0108 // 成员隐藏继承的成员；缺少关键字 new
        public string TextBeforeCursor => null;
        public string TextAfterCursor => null;


        private TextSelection textSelection; 
        public override TextSelection Selection { get => textSelection; set => textSelection=value; }

        private void OnCaretIndexChanged(int index) => CursorRectangleChanged?.Invoke(this, EventArgs.Empty);

        public void SetPresenter(TextPresenter presenter)
        {
            _subscription?.Dispose();
            _subscription = null;
            _presenter = presenter;
            if (_presenter != null)
            {
                _subscription = _presenter.GetObservable(TextPresenter.CaretIndexProperty)
                    .Subscribe(OnCaretIndexChanged);
            }
            TextViewVisualChanged?.Invoke(this, EventArgs.Empty);
            CursorRectangleChanged?.Invoke(this, EventArgs.Empty);
        }

        //设置输入框位置
        private double x, y;
        public void SetPosition(Point pos)
        {
            x = pos.X;
            y = pos.Y;
            CursorRectangleChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
