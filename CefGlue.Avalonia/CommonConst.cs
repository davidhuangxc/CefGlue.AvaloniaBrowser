﻿namespace CefGlue.Avalonia
{
    public class CommonConst
    {
        public const int CefDebugPort = 8200;

        public static bool IsDebug { get; set; } = false;

        public static int HttpPort { get; set; } = 4600;

        public const string WebapiPath = "/api/Execute";
    }
}
