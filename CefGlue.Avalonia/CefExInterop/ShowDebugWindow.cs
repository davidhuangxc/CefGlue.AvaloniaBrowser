﻿using Avalonia;
using Avalonia.Threading;

namespace CefGlue.Avalonia.CefExInterop
{
    public class ShowLinuxDebugWindow
    {
        public static async void ShowDebugWindow(int debugPort)
        {
            await Dispatcher.UIThread.InvokeAsync(async () =>
            {
                var messageBoxStandardWindow = MsBox.Avalonia.MessageBoxManager
                    .GetMessageBoxStandard("提示", $"打开任何基于chromium内核的浏览器，在地址栏输入\"http://127.0.0.1:{debugPort}\"即可调试页面");
                await messageBoxStandardWindow.ShowWindowDialogAsync(Application.Current.GetMainWindow());
            });
            //string command = $"google-chrome \"http://127.0.0.1:{debugPort}\"";
            //using Process p = new Process();
            ////设置要启动的应用程序
            //p.StartInfo.FileName = "bash";
            ////是否使用操作系统shell启动
            //p.StartInfo.UseShellExecute = false;
            //// 接受来自调用程序的输入信息
            //p.StartInfo.RedirectStandardInput = true;
            ////输出信息
            //p.StartInfo.RedirectStandardOutput = true;
            //// 输出错误
            //p.StartInfo.RedirectStandardError = false;
            ////不显示程序窗口
            //p.StartInfo.CreateNoWindow = true;
            //p.Start();
            //p.StandardInput.WriteLine(command);
            //p.StandardInput.Close();
        }
    }
}
