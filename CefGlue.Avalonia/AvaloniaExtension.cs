﻿using Avalonia.Controls;
using Avalonia.Diagnostics;

namespace CefGlue.Avalonia
{
    public static class AvaloniaExtension
    {
        public static void AttachDevTools(this TopLevel topLevel)
        {
            DevToolsOptions devToolsOptions = new DevToolsOptions()
            {
                StartupScreenIndex = 0,
            };
            //DevTools.Attach(topLevel,devToolsOptions);
        }
    }
}
