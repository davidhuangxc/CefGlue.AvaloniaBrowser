﻿using Avalonia;
using Avalonia.Threading;
using MsBox.Avalonia;
using MsBox.Avalonia.Enums;
using MsBox.Avalonia.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xilium.CefGlue;
using MessageBoxAvaloniaEnums = MsBox.Avalonia.Enums;

namespace CefGlue.Avalonia
{
    public class WpfCefJSDialogHandler : CefJSDialogHandler
    {
        private volatile Boolean IsCallReturned = false;
        protected override bool OnJSDialog(CefBrowser browser, string originUrl, CefJSDialogType dialogType, string message_text, string default_prompt_text, CefJSDialogCallback callback, out bool suppress_message)
        {
#pragma warning disable CS0219 // 变量已被赋值，但从未使用过它的值
            bool success = false;
#pragma warning restore CS0219 // 变量已被赋值，但从未使用过它的值
#pragma warning disable CS0219 // 变量已被赋值，但从未使用过它的值
            string input = null;
#pragma warning restore CS0219 // 变量已被赋值，但从未使用过它的值

            switch (dialogType)
            {
                case CefJSDialogType.Alert:
                    this.ShowJSAlert(message_text, callback);
                    break;
                case CefJSDialogType.Confirm:
                    this.ShowJSConfirm(message_text, callback);
                    break;
                case CefJSDialogType.Prompt:
                    this.ShowJSPromt(message_text, default_prompt_text, callback);
                    break;
            }

            //callback.Continue(success, input);
            suppress_message = false;
            return false;
        }
        private async Task TestComlete()
        {
            while (!IsCallReturned)
            {
                await Task.Delay(100);
            }
        }
        protected override bool OnBeforeUnloadDialog(CefBrowser browser, string messageText, bool isReload, CefJSDialogCallback callback)
        {
            return true;
        }

        protected override void OnDialogClosed(CefBrowser browser)
        {
        }



        protected override void OnResetDialogState(CefBrowser browser)
        {

        }

        private async void ShowJSAlert(string message, CefJSDialogCallback callback)
        {
            //WpfCefJSAlert alert = new WpfCefJSAlert(message);
            //alert.ShowDialog(Application.Current.GetMainWindow());
            //return true;
            ButtonResult buttonResult = ButtonResult.None;

            await Dispatcher.UIThread.InvokeAsync(async () =>
            {
                var messageBoxStandardWindow = MsBox.Avalonia.MessageBoxManager
                    .GetMessageBoxStandard("Message", message);
                buttonResult = await messageBoxStandardWindow.ShowWindowDialogAsync(Application.Current.GetMainWindow());
                callback.Continue(buttonResult == ButtonResult.Ok, "");
                IsCallReturned = true;
            });


            //return alert.ShowDialog() == true;
        }

        private async void ShowJSConfirm(string message, CefJSDialogCallback callback)
        {
#pragma warning disable CS0219 // 变量已被赋值，但从未使用过它的值
            ButtonResult buttonResult = ButtonResult.None;
#pragma warning restore CS0219 // 变量已被赋值，但从未使用过它的值
            await Dispatcher.UIThread.InvokeAsync(async () =>
            {
                var messageBoxStandardWindow = MessageBoxManager
               .GetMessageBoxStandard("Tips", message, MessageBoxAvaloniaEnums.ButtonEnum.YesNo, MessageBoxAvaloniaEnums.Icon.Question);

                var buttonResult = await messageBoxStandardWindow.ShowAsync();
                callback.Continue(buttonResult == ButtonResult.Ok, "");
                IsCallReturned = true;
            });


            //return confirm.ShowDialog() == true;
        }

        private async void ShowJSPromt(string message, string defaultText, CefJSDialogCallback callback)
        {
            //WpfCefJSPrompt promt = new WpfCefJSPrompt(message, defaultText);
            ///*if (promt.ShowDialog() == true)
            //{
            //    input = promt.Input;
            //    return true;
            //}
            //else
            //{
            //    input = null;
            //    return false;
            //}*/

            //input = "not implemented.";
            string input = "";
            await Dispatcher.UIThread.InvokeAsync(async () =>
            {
                var msgParam = new MsBox.Avalonia.Dto.MessageBoxCustomParams()
                {
                    ContentMessage = message,
                    ContentTitle = defaultText,
                    ButtonDefinitions = new List<ButtonDefinition>
                    {
                        new ButtonDefinition { Name = "Yes", },
                        new ButtonDefinition { Name = "No", },
                        new ButtonDefinition { Name = "Cancel", }
                    }
                };
                msgParam.InputParams = new MsBox.Avalonia.Dto.InputParams()
                {
                    Multiline = true,
                    DefaultValue = defaultText
                };
                msgParam.InputParams.DefaultValue = defaultText;
                var messageBoxStandardWindow = MessageBoxManager
                    .GetMessageBoxCustom(msgParam);
                var buttonResult = await messageBoxStandardWindow.ShowAsPopupAsync(Application.Current.GetMainWindow());

                input = messageBoxStandardWindow.InputValue;
                callback.Continue(buttonResult.Contains("yes",StringComparison.CurrentCultureIgnoreCase), input);
                IsCallReturned = true;
            });

        }
    }
}
