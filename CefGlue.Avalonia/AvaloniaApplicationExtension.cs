﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using System.Linq;

namespace CefGlue.Avalonia
{
    public static class AvaloniaApplicationExtension
    {
        public static Window GetMainWindow(this Application application)
        {
            var classif = Application.Current.ApplicationLifetime as IClassicDesktopStyleApplicationLifetime;
            return classif.MainWindow;
        }

#pragma warning disable CS8632 // 只能在 "#nullable" 注释上下文内的代码中使用可为 null 的引用类型的注释。
        public static Window? GetActiveWindow(this Application application)
#pragma warning restore CS8632 // 只能在 "#nullable" 注释上下文内的代码中使用可为 null 的引用类型的注释。
        {
            var classif = Application.Current.ApplicationLifetime as IClassicDesktopStyleApplicationLifetime;
            return classif.Windows.FirstOrDefault(t => t.IsActive);
        }
    }
}
