﻿using System;
using Xilium.CefGlue;

namespace CefGlue.Avalonia
{
    internal sealed class SampleCefApp : CefApp
    {
        public event EventHandler WebKitInitialized;
        public event RegisterCustomSchemesHandler RegisterCustomSchemes;

        public SampleCefApp()
        {
        }

        protected override void OnBeforeCommandLineProcessing(string processType, CefCommandLine commandLine)
        {
            if (string.IsNullOrEmpty(processType))
            {
                commandLine.AppendSwitch("disable-gpu");
                commandLine.AppendSwitch("disable-gpu-compositing");
                commandLine.AppendSwitch("enable-begin-frame-scheduling");
                commandLine.AppendSwitch("disable-smooth-scrolling");
            }
            if (CefRuntime.Platform == CefRuntimePlatform.Linux)
            {
                commandLine.AppendSwitch("disable-gpu", "1");
                commandLine.AppendSwitch("no-zygote", "1");
            }
            commandLine.AppendSwitch("no-proxy-server"); //禁止代理
            if (CommonConst.IsDebug)
            {
                commandLine.AppendSwitch("single-process");
            }

            commandLine.AppendSwitch("enable-devtools-experiments");
            commandLine.AppendSwitch("ignore-certificate-errors");
            commandLine.AppendSwitch("enable-begin-frame-scheduling");
            commandLine.AppendSwitch("enable-media-stream");
            commandLine.AppendSwitch("enable-blink-features", "CSSPseudoHas");
        }

        private CefBrowserProcessHandler _browserProcessHandler;
        private AvaloniaCefRenderProcessHandler CefRenderProcessHandler;

        protected override CefBrowserProcessHandler GetBrowserProcessHandler()
        {
            if (_browserProcessHandler == null)
            {
                _browserProcessHandler = new AvaloniaCefBrowserProcessHandler();
            }

            return _browserProcessHandler;
        }

        protected override CefRenderProcessHandler GetRenderProcessHandler()
        {
            if (CefRenderProcessHandler == null)
            {
                CefRenderProcessHandler = new AvaloniaCefRenderProcessHandler();
                CefRenderProcessHandler.WebKitInitialized += Handler_WebKitInitialized;
            }
            return CefRenderProcessHandler;
        }

        private void Handler_WebKitInitialized(object sender, System.EventArgs e)
        {
            WebKitInitialized?.Invoke(sender, e);
        }

        protected override void OnRegisterCustomSchemes(CefSchemeRegistrar registrar)
        {
            RegisterCustomSchemes?.Invoke(this, new RegisterCustomSchemesEventArgs(registrar));
        }
    }
}
