﻿using NLog;
using System;

namespace CefGlue.Avalonia
{
    public static class LogHelper
    {
        static Logger logger = LogManager.GetLogger("*");

        #region info
        public static void Info(string message)
        {
            logger.Info(message); LogManager.Flush();
        }
        public static void Info(string message, params object[] args)
        {
            logger.Info(message, args); LogManager.Flush();
        }
        public static void Info(Exception ex, string message)
        {
            logger.Info(ex, message); LogManager.Flush();
        }
        public static void Info(Exception ex, string message, params object[] args)
        {
            logger.Info(ex, message, args); LogManager.Flush();
        }
        #endregion

        #region error
        public static void Error(string message)
        {
            logger.Error(message); LogManager.Flush();
        }
        public static void Error(string message, params object[] args)
        {
            logger.Error(message, args); LogManager.Flush();
        }
        public static void Error(Exception ex, string message)
        {
            logger.Error(ex, message); LogManager.Flush();
        }
        public static void Error(Exception ex, string message, params object[] args)
        {
            logger.Error(ex, message, args); LogManager.Flush();
        }
        #endregion

        #region debug
        public static void Debug(string message)
        {
            logger.Debug(message);
            LogManager.Flush();
        }
        public static void Debug(string message, params object[] args)
        {
            logger.Debug(message, args); LogManager.Flush();
        }
        public static void Debug(Exception ex, string message)
        {
            logger.Debug(ex, message); LogManager.Flush();
        }
        public static void Debug(Exception ex, string message, params object[] args)
        {
            logger.Debug(ex, message, args); LogManager.Flush();
        }
        #endregion
    }
}
