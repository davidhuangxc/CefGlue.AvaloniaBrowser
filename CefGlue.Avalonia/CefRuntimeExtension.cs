﻿using System;
using System.Linq;
using Xilium.CefGlue;

namespace CefGlue.Avalonia
{
    public static class CefRuntimeExtension
    {
        /// <summary>
        /// 根据启动参数判断是否为启动cef子进程
        /// 该函数应当在应用程序的入口函数处被调用，用以执行一个子进程。它可以用于执行一个可执行程序来启动一个子进程，
        /// 该可执行程序可以是当前的浏览器客户端可执行程序（默认行为）或是通过设置CefSettings.browser_subprocess_path指定路径的可执行程序。
        /// 如果被调用用于浏览器进程（在启动命令行中没有"type"参数），该函数会立刻返回-1。如果被调用时识别为子进程，
        /// 该函数将会阻塞直到子进程退出并且返回子进程退出的返回码。
        /// application参数可以为空（null）。windows_sandbox_info参数只能在Windows上使用或设置为NULL（详见cef_sandbox_win.h）
        /// 摘自 https://zhuanlan.zhihu.com/p/365414447
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static bool IsSecondaryProcess(string[] args)
        {
            if (args.Any(o => o.StartsWith("--type")))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// 启动cef的子进程，判断启动参数里面有子进程的标记，则启动cef的子进程
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static bool RunSecondaryProcess(string[] args)
        {
            //SetDllImportResolver();
            CefRuntime.Load();
            var cefArgs = new CefMainArgs(TransformArgs(args));
            var cefApp = new SampleCefApp();

            //cefApp.RegisterCustomSchemes += CefApp_RegisterCustomSchemes;
            //cefApp.WebKitInitialized += CefApp_WebKitInitialized;
            var code = CefRuntime.ExecuteProcess(cefArgs, cefApp, IntPtr.Zero);
            if (code >= 0) return true;
            return false;
        }

        private static void CefApp_WebKitInitialized(object sender, EventArgs e)
        {
            AvaloniaCefBrowser.OnWebKitInitialized(sender, e);
        }

        private static void CefApp_RegisterCustomSchemes(object sender, RegisterCustomSchemesEventArgs e)
        {
            AvaloniaCefBrowser.OnRegisterCustomSchemes(sender, e);
        }


        public static void Shutdown()
        {
            CefRuntime.Shutdown();
        }


        /// <summary>
        /// 启动参数转换
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private static string[] TransformArgs(string[] args)
        {
            var argv = args;
            if (CefRuntime.Platform == CefRuntimePlatform.Linux)
            {
                argv = new string[args.Length + 1];
                Array.Copy(args, 0, argv, 1, args.Length);
                argv[0] = "-";
            }

            return argv;
        }
    }
}
