using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Input;
using Avalonia.Media;
using Avalonia.Media.Imaging;
using Avalonia.Platform;
using Avalonia.Threading;
using Avalonia.VisualTree;
using CefGlue.Avalonia.Handlers;
using CefGlue.Avalonia.Javascript;
using System.Linq;
using System.Runtime.CompilerServices;
using System;
using System.Threading.Tasks;
using Xilium.CefGlue;
using CefGlue.Avalonia.Interop;
using Avalonia.Interactivity;
using System.Collections;

namespace CefGlue.Avalonia
{
    public partial class AvaloniaCefBrowser : UserControl
    {
#pragma warning disable CS0169 // 从不使用字段“AvaloniaCefBrowser._disposed”
        private bool _disposed;
#pragma warning restore CS0169 // 从不使用字段“AvaloniaCefBrowser._disposed”
        private bool _created;

        private Image _browserPageImage;

        private WriteableBitmap _browserPageBitmap;

        private int _browserWidth;
        private int _browserHeight;
        private bool _browserSizeChanged;

        private CefBrowser _browser;
        private CefBrowserHost _browserHost;
        private WpfCefClient _cefClient;

#pragma warning disable CS0649 // 从未对字段“AvaloniaCefBrowser._tooltip”赋值，字段将一直保持其默认值 null
        private ToolTip _tooltip;
#pragma warning restore CS0649 // 从未对字段“AvaloniaCefBrowser._tooltip”赋值，字段将一直保持其默认值 null
#pragma warning disable CS0169 // 从不使用字段“AvaloniaCefBrowser._tooltipTimer”
        private DispatcherTimer _tooltipTimer;
#pragma warning restore CS0169 // 从不使用字段“AvaloniaCefBrowser._tooltipTimer”

        private Panel _popup;
        private Image _popupImage;
        private WriteableBitmap _popupImageBitmap;

        private TaskCompletionSource<string> _messageReceiveCompletionSource;

        public string StartUrl { get; set; }
        public bool AllowsTransparency { get; set; } = false;
        public Key Keys { get; private set; }


        public CefBrowser Browser => _browser;
        public JavascriptRepository JavascriptRepository { get; set; } = new JavascriptRepository();

        private MyTextBoxTextInputMethodClient _imClient = new MyTextBoxTextInputMethodClient();
        static AvaloniaCefBrowser()
        {
            TextInputMethodClientRequestedEvent.AddClassHandler<AvaloniaCefBrowser>((tb, e) =>
            {
                e.Client = tb._imClient;
            });
        }
        public AvaloniaCefBrowser()
        {
            InitializeComponent();
            CreatePopup();
        }

        private void CreatePopup()
        {
            var popup = this.FindControl<Grid>("Popup");
            this._popupImage = this.FindControl<Image>("listPopupImage");
            _popupImage.Stretch = Stretch.None;


            _popupImageBitmap = new WriteableBitmap(
                             new PixelSize(1, 1),
                             new Vector(96, 96),
                              PixelFormat.Bgra8888, AlphaFormat.Opaque);

            _popupImage.Source = this._popupImageBitmap;
            _popup = popup;
        }

        protected override void OnTextInput(TextInputEventArgs e)
        {
            foreach (char symbol in e.Text)
            {
                CefNet.CefEventFlags modifiers = CefNet.Input.KeycodeConverter.IsShiftRequired(symbol) ? CefNet.CefEventFlags.ShiftDown : CefNet.CefEventFlags.None;

                if (symbol >= 0x4e00 && symbol <= 0x9fff)
                {
                    var k = new CefKeyEvent();
                    k.EventType = CefKeyEventType.Char;
                    k.WindowsKeyCode = symbol;
                    k.Character = symbol;
                    k.UnmodifiedCharacter = symbol;
                    k.Modifiers = (CefEventFlags)((uint)modifiers);
                    k.NativeKeyCode = symbol;
                    this._browserHost.SendKeyEvent(k);
                }
                else
                {
                    CefNet.Input.VirtualKeys key = CefNet.Input.KeycodeConverter.Default.CharacterToVirtualKey(symbol);

                    var k = new CefKeyEvent();
                    k.EventType = CefKeyEventType.Char;
                    k.WindowsKeyCode = CefRuntime.Platform == CefRuntimePlatform.Linux ? (int)key : symbol;
                    k.Character = symbol;
                    k.UnmodifiedCharacter = symbol;
                    k.Modifiers = (CefEventFlags)((uint)modifiers); ;
                    k.NativeKeyCode = CefNet.Input.KeycodeConverter.Default.VirtualKeyToNativeKeyCode(key, modifiers, false);
                    this._browserHost.SendKeyEvent(k);
                }
            }
            e.Handled = true;
        }

        protected override void OnPointerPressed(PointerPressedEventArgs e)
        {
            var mousePos = e.GetPosition(this);
            _imClient.SetPosition(mousePos);

            base.OnPointerPressed(e);
        }
        protected override void OnApplyTemplate(TemplateAppliedEventArgs e)
        {
            base.OnApplyTemplate(e);
            _browserPageImage = this.FindControl<Image>("PART_Image");
        }
        //protected override void OnTemplateApplied(TemplateAppliedEventArgs e)
        //{
        //    base.OnTemplateApplied(e);

        //    _browserPageImage = e.NameScope.Find<Image>("PART_Image");
        //}

        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            var size = base.ArrangeOverride(arrangeBounds);

            if (_browserPageImage != null)
            {
                var newWidth = (int)size.Width;
                var newHeight = (int)size.Height;

                if (newWidth > 0 && newHeight > 0)
                {
                    if (!_created)
                    {
                        AttachEventHandlers(this); // TODO: ?

                        // Create the bitmap that holds the rendered website bitmap
                        _browserWidth = newWidth;
                        _browserHeight = newHeight;
                        _browserSizeChanged = true;

                        // Find the window that's hosting us                        
                        Window parentWnd = this.GetVisualRoot() as Window;

                        if (parentWnd != null)
                        {

                            IntPtr hParentWnd = parentWnd.TryGetPlatformHandle().Handle;

                            var windowInfo = CefWindowInfo.Create();
                            windowInfo.SetAsWindowless(hParentWnd, AllowsTransparency);

                            var settings = new CefBrowserSettings();
                            _cefClient = new WpfCefClient(this);

                            _messageReceiveCompletionSource = new TaskCompletionSource<string>();

                            _cefClient.MessageReceived += (sender, e) =>
                            {
                                if (e.Message.Name == "executeJsResult")
                                {
                                    _messageReceiveCompletionSource.SetResult(e.Message.Arguments.GetString(0));
                                }
                            };

                            // This is the first time the window is being rendered, so create it.
                            CefBrowserHost.CreateBrowser(windowInfo, _cefClient, settings, !string.IsNullOrEmpty(StartUrl) ? StartUrl : "about:blank", CreateJavascriptRegisterInfo());

                            _created = true;
                        }
                    }
                    else
                    {
                        // Only update the bitmap if the size has changed
                        if (_browserPageBitmap == null || (_browserPageBitmap.PixelSize.Width != newWidth || _browserPageBitmap.PixelSize.Height != newHeight))
                        {
                            _browserWidth = newWidth;
                            _browserHeight = newHeight;
                            _browserSizeChanged = true;

                            // If the window has already been created, just resize it
                            if (_browserHost != null)
                            {
                                _browserHost.WasResized();
                            }
                        }
                    }
                }
            }

            return size;

        }

        private static CefEventFlags GetKeyboardModifiers(KeyModifiers keyboardModifiers)
        {
            var modifiers = new CefEventFlags();

            if (keyboardModifiers.HasFlag(KeyModifiers.Alt))
            {
                modifiers |= CefEventFlags.AltDown;
            }

            if (keyboardModifiers.HasFlag(KeyModifiers.Control))
            {
                modifiers |= CefEventFlags.ControlDown;
            }

            if (keyboardModifiers.HasFlag(KeyModifiers.Shift))
            {
                modifiers |= CefEventFlags.ShiftDown;
            }

            return modifiers;
        }

        private static CefEventFlags GetMouseModifiers(KeyModifiers keyboardModifiers, PointerPointProperties mouseModifiers)
        {
            var modifiers = new CefEventFlags();

            if (mouseModifiers.IsLeftButtonPressed)
            {
                modifiers |= CefEventFlags.LeftMouseButton;
            }

            if (mouseModifiers.IsMiddleButtonPressed)
            {
                modifiers |= CefEventFlags.ControlDown;
            }

            if (mouseModifiers.IsRightButtonPressed)
            {
                modifiers |= CefEventFlags.RightMouseButton;
            }

            return modifiers;
        }
        #region 事件处理
        private void GotFocusHandler(object? sender, GotFocusEventArgs arg)
        {
#pragma warning disable CS0168 // 声明了变量，但从未使用过
            try
            {
                if (_browserHost != null)
                {
                    InputMethod.SetIsInputMethodEnabled(this, true);
                    _browserHost.SetFocus(true);
                }
            }
            catch (Exception ex)
            {

            }
#pragma warning restore CS0168 // 声明了变量，但从未使用过
        }
        private void LostFocusHandler(object? sender, RoutedEventArgs arg)
        {
#pragma warning disable CS0168 // 声明了变量，但从未使用过
            try
            {
                if (_browserHost != null)
                {
                    InputMethod.SetIsInputMethodEnabled(this, false);
                    _browserHost.SetFocus(false);
                }
            }
            catch (Exception ex)
            {

            }
#pragma warning restore CS0168 // 声明了变量，但从未使用过
        }

        private void PointerExitedHandler(object? sender, PointerEventArgs arg)
        {
#pragma warning disable CS0168 // 声明了变量，但从未使用过
            try
            {
                if (_browserHost != null)
                {
                    CefMouseEvent mouseEvent = new CefMouseEvent()
                    {
                        X = 0,
                        Y = 0
                    };

                    mouseEvent.Modifiers = GetMouseModifiers(arg.KeyModifiers, arg.GetCurrentPoint(this).Properties);

                    _browserHost.SendMouseMoveEvent(mouseEvent, true);
                    //_logger.Debug("Browser_MouseLeave");
                }
            }
            catch (Exception ex)
            {

            }
#pragma warning restore CS0168 // 声明了变量，但从未使用过
        }

        private void PointerMovedHandler(object? sender, PointerEventArgs arg)
        {
#pragma warning disable CS0168 // 声明了变量，但从未使用过
            try
            {
                if (_browserHost != null)
                {
                    Point cursorPos = arg.GetPosition(this);

                    CefMouseEvent mouseEvent = new CefMouseEvent()
                    {
                        X = (int)cursorPos.X,
                        Y = (int)cursorPos.Y
                    };

                    mouseEvent.Modifiers = GetMouseModifiers(arg.KeyModifiers, arg.GetCurrentPoint(this).Properties);

                    _browserHost.SendMouseMoveEvent(mouseEvent, false);

                    //_logger.Debug(string.Format("Browser_MouseMove: ({0},{1})", cursorPos.X, cursorPos.Y));
                }
            }
            catch (Exception ex)
            {

            }
#pragma warning restore CS0168 // 声明了变量，但从未使用过
        }

        private void PointerPressedHandler(object? sender, PointerPressedEventArgs arg)
        {
#pragma warning disable CS0168 // 声明了变量，但从未使用过
            try
            {
                if (_browserHost != null)
                {
                    Focus();

                    Point cursorPos = arg.GetPosition(this);

                    CefMouseEvent mouseEvent = new CefMouseEvent()
                    {
                        X = (int)cursorPos.X,
                        Y = (int)cursorPos.Y,
                    };
                    PointerPointProperties point = arg.GetCurrentPoint(this).Properties;
                    mouseEvent.Modifiers = GetMouseModifiers(arg.KeyModifiers, point);

                    if (mouseEvent.Modifiers == CefEventFlags.LeftMouseButton)
                        _browserHost.SendMouseClickEvent(mouseEvent, CefMouseButtonType.Left, false, arg.ClickCount);
                    else if (mouseEvent.Modifiers == CefEventFlags.MiddleMouseButton)
                        _browserHost.SendMouseClickEvent(mouseEvent, CefMouseButtonType.Middle, false, arg.ClickCount);
                    else if (mouseEvent.Modifiers == CefEventFlags.RightMouseButton)
                        _browserHost.SendMouseClickEvent(mouseEvent, CefMouseButtonType.Right, false, arg.ClickCount);

                    //_logger.Debug(string.Format("Browser_MouseDown: ({0},{1})", cursorPos.X, cursorPos.Y));
                }
            }
            catch (Exception ex)
            {

            }
#pragma warning restore CS0168 // 声明了变量，但从未使用过
        }

        private void PointerReleasedHandler(object? sender, PointerReleasedEventArgs arg)
        {
#pragma warning disable CS0168 // 声明了变量，但从未使用过
            try
            {
                if (_browserHost != null)
                {
                    Point cursorPos = arg.GetPosition(this);

                    CefMouseEvent mouseEvent = new CefMouseEvent()
                    {
                        X = (int)cursorPos.X,
                        Y = (int)cursorPos.Y,
                    };

                    mouseEvent.Modifiers = GetMouseModifiers(arg.KeyModifiers, arg.GetCurrentPoint(this).Properties);

                    if (arg.InitialPressMouseButton == MouseButton.Left)
                        _browserHost.SendMouseClickEvent(mouseEvent, CefMouseButtonType.Left, true, 1);
                    else if (arg.InitialPressMouseButton == MouseButton.Middle)
                        _browserHost.SendMouseClickEvent(mouseEvent, CefMouseButtonType.Middle, true, 1);
                    else if (arg.InitialPressMouseButton == MouseButton.Right)
                        _browserHost.SendMouseClickEvent(mouseEvent, CefMouseButtonType.Right, true, 1);

                    //_logger.Debug(string.Format("Browser_MouseUp: ({0},{1})", cursorPos.X, cursorPos.Y));
                }
            }
            catch (Exception ex)
            {
                //_logger.ErrorException("WpfCefBrowser: Caught exception in MouseUp()", ex);
            }
#pragma warning restore CS0168 // 声明了变量，但从未使用过
        }

        private void PointerWheelChangedHandler(object? sender, PointerWheelEventArgs arg)
        {
#pragma warning disable CS0168 // 声明了变量，但从未使用过
            try
            {
                if (_browserHost != null)
                {
                    Point cursorPos = arg.GetPosition(this);

                    CefMouseEvent mouseEvent = new CefMouseEvent()
                    {
                        X = (int)cursorPos.X,
                        Y = (int)cursorPos.Y,
                    };

                    _browserHost.SendMouseWheelEvent(mouseEvent, 0, (int)arg.Delta.Y);
                    _browser.GetMainFrame().ExecuteJavaScript($"var offsetX=window.pageXOffset  + {arg.Delta.X}; var offsetY=window.pageYOffset+{arg.Delta.Y*25};  window.scrollTo(offsetX,offsetY);",Browser.GetMainFrame().Url,0);
                }
            }
            catch (Exception ex)
            {
                //_logger.ErrorException("WpfCefBrowser: Caught exception in MouseWheel()", ex);
            }
#pragma warning restore CS0168 // 声明了变量，但从未使用过
        }

        private void TextInputHandler(object? sender, TextInputEventArgs arg)
        {
            if (_browserHost != null)
            {
                foreach (var c in arg.Text)
                {
                    CefKeyEvent keyEvent = new CefKeyEvent()
                    {
                        EventType = CefKeyEventType.Char,
                        WindowsKeyCode = (int)c,
                        Character = c,
                        FocusOnEditableField = true,
                    };

                    //keyEvent.Modifiers = GetKeyboardModifiers(KeyboardDevice.Instance.);

                    _browserHost.SendKeyEvent(keyEvent);
                }
            }

            arg.Handled = true;
        }

        private void KeyDownHandler(object? sender, KeyEventArgs arg)
        {
#pragma warning disable CS0168 // 声明了变量，但从未使用过
            try
            {
                if (_browserHost != null)
                {
                    //_logger.Debug(string.Format("KeyDown: system key {0}, key {1}", arg.SystemKey, arg.Key));
                    CefKeyEvent keyEvent = new CefKeyEvent()
                    {
                        EventType = CefKeyEventType.RawKeyDown,
                        WindowsKeyCode = KeyInterop.VirtualKeyFromKey(arg.Key),
                        NativeKeyCode = 0,
                        IsSystemKey = arg.Key == Key.System,
                    };

                    if (arg.Key == Key.Enter)
                    {
                        keyEvent.EventType = CefKeyEventType.Char;
                    }

                    keyEvent.Modifiers = GetKeyboardModifiers(arg.KeyModifiers);

                    _browserHost.SendKeyEvent(keyEvent);
                }
            }
            catch (Exception ex)
            {
                //_logger.ErrorException("WpfCefBrowser: Caught exception in PreviewKeyDown()", ex);
            }
#pragma warning restore CS0168 // 声明了变量，但从未使用过

            //arg.Handled = HandledKeys.Contains(arg.Key);
        }
        private void KeyUpHandler(object? sender, KeyEventArgs arg)
        {
#pragma warning disable CS0168 // 声明了变量，但从未使用过
            try
            {
                if (_browserHost != null)
                {
                    //_logger.Debug(string.Format("KeyUp: system key {0}, key {1}", arg.SystemKey, arg.Key));
                    CefKeyEvent keyEvent = new CefKeyEvent()
                    {
                        EventType = CefKeyEventType.KeyUp,
                        WindowsKeyCode = KeyInterop.VirtualKeyFromKey(arg.Key),
                        NativeKeyCode = 0,
                        IsSystemKey = arg.Key == Key.System,
                    };

                    keyEvent.Modifiers = GetKeyboardModifiers(arg.KeyModifiers);

                    _browserHost.SendKeyEvent(keyEvent);
                }
            }
            catch (Exception ex)
            {
                //_logger.ErrorException("WpfCefBrowser: Caught exception in PreviewKeyUp()", ex);
            }
#pragma warning restore CS0168 // 声明了变量，但从未使用过

            arg.Handled = true;

            var location = System.Reflection.Assembly.GetEntryAssembly().Location;
            var directory = System.IO.Path.GetDirectoryName(location);
        }
        #endregion
        private void AttachEventHandlers(AvaloniaCefBrowser browser)
        {
            browser.IsEnabled = true;
            browser.Focusable = true;
            browser.PART_Image.IsEnabled = true;
            browser.PART_Image.Focusable = true;
            browser.GotFocus += new EventHandler<GotFocusEventArgs>(GotFocusHandler);

            browser.LostFocus += new EventHandler<RoutedEventArgs>(LostFocusHandler);
            browser.PointerExited += new EventHandler<PointerEventArgs>(PointerExitedHandler);

            browser.PointerMoved += new EventHandler<PointerEventArgs>(PointerMovedHandler);
            browser.PointerPressed += new EventHandler<PointerPressedEventArgs>(PointerPressedHandler);

            browser.PointerReleased += new EventHandler<PointerReleasedEventArgs>(PointerReleasedHandler);

            browser.PointerWheelChanged += new EventHandler<PointerWheelEventArgs>(PointerWheelChangedHandler);

            // TODO: require more intelligent processing
            browser.TextInput += new EventHandler<TextInputEventArgs>(TextInputHandler);

            // TODO: require more intelligent processing
            browser.KeyDown += new EventHandler<KeyEventArgs>(KeyDownHandler);

            // TODO: require more intelligent processing
            browser.KeyUp += new EventHandler<KeyEventArgs>(KeyUpHandler);

            /*browser._popup.MouseMove += (sender, arg) =>
            {
                try
                {
                    if (_browserHost != null)
                    {
                        Point cursorPos = arg.GetPosition(this);

                        CefMouseEvent mouseEvent = new CefMouseEvent()
                        {
                            X = (int)cursorPos.X,
                            Y = (int)cursorPos.Y
                        };

                        mouseEvent.Modifiers = GetMouseModifiers();

                        _browserHost.SendMouseMoveEvent(mouseEvent, false);

                        //_logger.Debug(string.Format("Popup_MouseMove: ({0},{1})", cursorPos.X, cursorPos.Y));
                    }
                }
                catch (Exception ex)
                {
                    _logger.ErrorException("WpfCefBrowser: Caught exception in Popup.MouseMove()", ex);
                }
            };

            browser._popup.MouseDown += (sender, arg) =>
            {
                try
                {
                    if (_browserHost != null)
                    {
                        Point cursorPos = arg.GetPosition(this);

                        CefMouseEvent mouseEvent = new CefMouseEvent()
                        {
                            X = (int)cursorPos.X,
                            Y = (int)cursorPos.Y
                        };

                        mouseEvent.Modifiers = GetMouseModifiers();

                        _browserHost.SendMouseClickEvent(mouseEvent, CefMouseButtonType.Left, true, 1);

                        //_logger.Debug(string.Format("Popup_MouseDown: ({0},{1})", cursorPos.X, cursorPos.Y));
                    }
                }
                catch (Exception ex)
                {
                    _logger.ErrorException("WpfCefBrowser: Caught exception in Popup.MouseDown()", ex);
                }
            };

            browser._popup.MouseWheel += (sender, arg) =>
            {
                try
                {
                    if (_browserHost != null)
                    {
                        Point cursorPos = arg.GetPosition(this);
                        int delta = arg.Delta;
                        CefMouseEvent mouseEvent = new CefMouseEvent()
                        {
                            X = (int)cursorPos.X,
                            Y = (int)cursorPos.Y
                        };

                        mouseEvent.Modifiers = GetMouseModifiers();
                        _browserHost.SendMouseWheelEvent(mouseEvent, 0, delta);

                        //_logger.Debug(string.Format("MouseWheel: ({0},{1})", cursorPos.X, cursorPos.Y));
                    }
                }
                catch (Exception ex)
                {
                    _logger.ErrorException("WpfCefBrowser: Caught exception in Popup.MouseWheel()", ex);
                }
            };*/
        }

        internal bool OnTooltip(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                // _tooltipTimer.Stop();
                UpdateTooltip(null);
            }
            else
            {
                /*   _tooltipTimer.Tick += (sender, args) => UpdateTooltip(text);
                   _tooltipTimer.Start();*/
            }

            return true;
        }

        public event LoadStartEventHandler LoadStart;
        public event LoadEndEventHandler LoadEnd;
        public event LoadingStateChangeEventHandler LoadingStateChange;
        public event LoadErrorEventHandler LoadError;
        public event BrowserCreatedHandler BrowserCreated;

        public static event EventHandler WebKitInitialized;
        public static void OnWebKitInitialized(object sender, EventArgs e)
        {
            WebKitInitialized?.Invoke(sender, e);
        }

        public static event RegisterCustomSchemesHandler RegisterCustomSchemes;
        public static void OnRegisterCustomSchemes(object sender, RegisterCustomSchemesEventArgs e)
        {
            RegisterCustomSchemes?.Invoke(sender, e);
        }

        internal void OnLoadStart(CefFrame frame)
        {
            if (this.LoadStart != null)
            {
                var e = new LoadStartEventArgs(frame);
                this.LoadStart(this, e);
            }
        }

        internal void OnLoadEnd(CefFrame frame, int httpStatusCode)
        {
            if (this.LoadEnd != null)
            {
                var e = new LoadEndEventArgs(frame, httpStatusCode);
                this.LoadEnd(this, e);
            }
        }

        private object _scriptLock = new object();

        public async Task<string> ExecuteScriptAsync(string code, string scriptUrl = null)
        {
            var message = CefProcessMessage.Create("executeJs");
            message.Arguments.SetString(0, code);
            message.Arguments.SetString(1, scriptUrl);

            _messageReceiveCompletionSource = new TaskCompletionSource<string>();

            //_browser.SendProcessMessage(CefProcessId.Renderer, message);
            _browser.GetMainFrame().ExecuteJavaScript(code, scriptUrl, 0);
            await _messageReceiveCompletionSource.Task;

            var messageReceived = _messageReceiveCompletionSource.Task.Result;

            return messageReceived;
        }

        internal void OnLoadingStateChange(bool isLoading, bool canGoBack, bool canGoForward)
        {
            if (this.LoadingStateChange != null)
            {
                var e = new LoadingStateChangeEventArgs(isLoading, canGoBack, canGoForward);
                this.LoadingStateChange(this, e);
            }
        }
        internal void OnLoadError(CefFrame frame, CefErrorCode errorCode, string errorText, string failedUrl)
        {
            if (this.LoadError != null)
            {
                var e = new LoadErrorEventArgs(frame, errorCode, errorText, failedUrl);
                this.LoadError(this, e);
            }
        }

        private void UpdateTooltip(string text)
        {
            Dispatcher.UIThread.InvokeAsync(
                    () =>
                    {
                        if (string.IsNullOrEmpty(text))
                        {
                            //_tooltip.IsOpen = false;
                        }
                        else
                        {
                            //_tooltip.Placement = PlacementMode.Mouse;
                            _tooltip.Content = text;
                            //_tooltip.IsOpen = true;
                            //_tooltip.Visibility = Visibility.Visible;
                        }
                    });

            //_tooltipTimer.Stop();
        }

        public void HandleAfterCreated(CefBrowser browser)
        {
            int width = 0, height = 0;

            bool hasAlreadyBeenInitialized = false;

            //Dispatcher.UIThread.InvokeTaskAsync(() =>
            {
                if (_browser != null)
                {
                    hasAlreadyBeenInitialized = true;
                }
                else
                {
                    _browser = browser;
                    _browserHost = _browser.GetHost();

                    // _browserHost.SetFocus(IsFocused);

                    width = (int)_browserWidth;
                    height = (int)_browserHeight;
                }
            }//);

            // Make sure we don't initialize ourselves more than once. That seems to break things.
            if (hasAlreadyBeenInitialized)
                return;

            if (width > 0 && height > 0)
                _browserHost.WasResized();

            // 			mainUiDispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate
            // 			{
            // 				if (!string.IsNullOrEmpty(this.initialUrl))
            // 				{
            // 					NavigateTo(this.initialUrl);
            // 					this.initialUrl = string.Empty;
            // 				}
            // 			}));

            this.BrowserCreated?.Invoke(this, new BrowserCreatedEventArgs(browser));
        }

        internal bool GetViewRect(out CefRectangle rect)
        {
            rect = new CefRectangle();
            bool rectProvided = false;
            CefRectangle browserRect = new CefRectangle();

            // TODO: simplify this
            //_mainUiDispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate
            //{
#pragma warning disable CS0168 // 声明了变量，但从未使用过
            try
            {
                // The simulated screen and view rectangle are the same. This is necessary
                // for popup menus to be located and sized inside the view.
                browserRect.X = browserRect.Y = 0;
                browserRect.Width = (int)_browserWidth;
                browserRect.Height = (int)_browserHeight;

                rectProvided = true;
            }
            catch (Exception ex)
            {
                rectProvided = false;
            }
#pragma warning restore CS0168 // 声明了变量，但从未使用过
            //}));

            if (rectProvided)
            {
                rect = browserRect;
            }

            return rectProvided;
        }

        internal void GetScreenPoint(int viewX, int viewY, ref int screenX, ref int screenY)
        {
            PixelPoint ptScreen = new PixelPoint();

            //Dispatcher.UIThread.InvokeAsync(()=>
            {
#pragma warning disable CS0168 // 声明了变量，但从未使用过
                try
                {
                    Point ptView = new Point(viewX, viewY);
                    ptScreen = this.PointToScreen(ptView);
                }
                catch (Exception ex)
                {

                }
#pragma warning restore CS0168 // 声明了变量，但从未使用过
            }//);

            screenX = ptScreen.X;
            screenY = ptScreen.Y;
        }

        internal void HandleViewPaint(CefBrowser browser, CefPaintElementType type, CefRectangle[] dirtyRects, IntPtr buffer, int width, int height)
        {
            // When browser size changed - we just skip frame updating.
            // This is dirty precheck to do not do Invoke whenever is possible.
            if (_browserSizeChanged && (width != _browserWidth || height != _browserHeight)) return;

            //Dispatcher.UIThread.InvokeAsync(()=>
            {
                // Actual browser size changed check.
                if (_browserSizeChanged && (width != _browserWidth || height != _browserHeight)) return;

#pragma warning disable CS0168 // 声明了变量，但从未使用过
                try
                {
                    Dispatcher.UIThread.InvokeAsync(() =>
                    {
                        if (_browserSizeChanged)
                        {
                            _browserPageBitmap = new WriteableBitmap(new PixelSize((int)_browserWidth, (int)_browserHeight), new Vector(96, 96), PixelFormat.Bgra8888, AlphaFormat.Opaque);//new WriteableBitmap((int)_browserWidth, (int)_browserHeight, 96, 96, AllowsTransparency ? PixelFormats.Bgra32 : PixelFormats.Bgr32, null);
                            _browserPageImage.Source = _browserPageBitmap;

                            _browserSizeChanged = false;
                        }

                        if (_browserPageBitmap != null)
                        {
                            DoRenderBrowser(_browserPageBitmap, width, height, dirtyRects, buffer);

                            _browserPageImage.InvalidateVisual();
                        }
                    });



                }
                catch (Exception ex)
                {
                }
#pragma warning restore CS0168 // 声明了变量，但从未使用过
            }//);
        }

        internal void HandlePopupPaint(int width, int height, CefRectangle[] dirtyRects, IntPtr sourceBuffer)
        {
            if (width == 0 || height == 0)
            {
                return;
            }

            Dispatcher.UIThread.InvokeAsync(() =>
            {
                //int stride = width * 4;
                //int sourceBufferSize = stride * height;

                //foreach (CefRectangle dirtyRect in dirtyRects)
                //{
                //    if (dirtyRect.Width == 0 || dirtyRect.Height == 0)
                //    {
                //        continue;
                //    }

                //    int adjustedWidth = dirtyRect.Width;

                //    int adjustedHeight = dirtyRect.Height;

                //    Rect sourceRect = new Rect(dirtyRect.X, dirtyRect.Y, adjustedWidth, adjustedHeight);

                //    // _popupImageBitmap.WritePixels(sourceRect, sourceBuffer, sourceBufferSize, stride, dirtyRect.X, dirtyRect.Y);
                //}
                UpdateBitmap(ref _popupImageBitmap, ref dirtyRects, ref sourceBuffer);
                _popupImage.InvalidateVisual();
                if (!_popup.IsVisible)
                {
                    _popup.IsVisible = true;
                }
            });
        }

        private unsafe void DoRenderBrowser(WriteableBitmap bitmap, int browserWidth, int browserHeight, CefRectangle[] dirtyRects, IntPtr sourceBuffer)
        {
            int stride = browserWidth * 4;
            int sourceBufferSize = stride * browserHeight;

            if (browserWidth == 0 || browserHeight == 0)
            {
                return;
            }
            using (var l = bitmap.Lock())
            {
                //byte[] managedArray = new byte[sourceBufferSize];

                //Marshal.Copy(sourceBuffer, managedArray, 0, sourceBufferSize);

                //Marshal.Copy(managedArray, 0, l.Address, sourceBufferSize);
                Buffer.MemoryCopy(sourceBuffer.ToPointer(), l.Address.ToPointer(), sourceBufferSize, sourceBufferSize);
            }
            //UpdateBitmap(ref bitmap, ref dirtyRects, ref sourceBuffer);
            //foreach (CefRectangle dirtyRect in dirtyRects)
            //{
            //    if (dirtyRect.Width == 0 || dirtyRect.Height == 0)
            //    {
            //        continue;
            //    }

            //    // If the window has been resized, make sure we never try to render too much
            //    int adjustedWidth = (int)dirtyRect.Width;
            //    //if (dirtyRect.X + dirtyRect.Width > (int) bitmap.Width)
            //    //{
            //    //    adjustedWidth = (int) bitmap.Width - (int) dirtyRect.X;
            //    //}

            //    int adjustedHeight = (int)dirtyRect.Height;
            //    //if (dirtyRect.Y + dirtyRect.Height > (int) bitmap.Height)
            //    //{
            //    //    adjustedHeight = (int) bitmap.Height - (int) dirtyRect.Y;
            //    //}

            //    // Update the dirty region
            //    var sourceRect = new Rect((int)dirtyRect.X, (int)dirtyRect.Y, adjustedWidth, adjustedHeight);


            //    //bitmap.WritePixels(sourceRect, sourceBuffer, sourceBufferSize, stride, (int)dirtyRect.X, (int)dirtyRect.Y);

            //    // 			int adjustedWidth = browserWidth;
            //    // 			if (browserWidth > (int)bitmap.Width)
            //    // 				adjustedWidth = (int)bitmap.Width;
            //    // 
            //    // 			int adjustedHeight = browserHeight;
            //    // 			if (browserHeight > (int)bitmap.Height)
            //    // 				adjustedHeight = (int)bitmap.Height;
            //    // 
            //    // 			int sourceBufferSize = browserWidth * browserHeight * 4;
            //    // 			int stride = browserWidth * 4;
            //    // 
            //    // 			Int32Rect sourceRect = new Int32Rect(0, 0, adjustedWidth, adjustedHeight);
            //    // 			bitmap.WritePixels(sourceRect, sourceBuffer, sourceBufferSize, stride, 0, 0);
            //}

            //using (var l = bitmap.Lock())
            //{
            //    byte[] managedArray = new byte[sourceBufferSize];

            //    Marshal.Copy(sourceBuffer, managedArray, 0, sourceBufferSize);

            //    Marshal.Copy(managedArray, 0, l.Address, sourceBufferSize);
            //}
        }
        /// <summary>
        /// 更新位图脏数据
        /// </summary>
        /// <param name="bitmap"></param>
        /// <param name="dirtyRects"></param>
        /// <param name="sourceBuffer"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        static unsafe void UpdateBitmap(ref WriteableBitmap bitmap, ref CefRectangle[] dirtyRects, ref IntPtr sourceBuffer)
        {
            if (!dirtyRects.Any()) return;
            if (sourceBuffer == IntPtr.Zero) return;
            var width = (int)bitmap.Size.Width;
            var height = (int)bitmap.Size.Height;
            int pixelByteLength = bitmap.Format.Value.BitsPerPixel >> 3;
            using (var l = bitmap.Lock())
            {
                byte* sp, dp;
                sp = (byte*)sourceBuffer;
                dp = (byte*)l.Address;

                foreach (var dirtyRect in dirtyRects)
                {
                    int adjustedWidth = dirtyRect.Width;
                    if (dirtyRect.X + dirtyRect.Width > width)
                    {
                        adjustedWidth = width - dirtyRect.X;
                    }

                    int adjustedHeight = dirtyRect.Height;
                    if (dirtyRect.Y + dirtyRect.Height > height)
                    {
                        adjustedHeight = height - dirtyRect.Y;
                    }
                    //对应实际正确的需要重绘制的像素区域，每个像素4个字节
                    // Update the dirty region
                    var sourceRect = new Rect(dirtyRect.X, dirtyRect.Y, adjustedWidth, adjustedHeight);

                    var stride = (int)sourceRect.Width * pixelByteLength;

                    for (var h = sourceRect.Y; h < sourceRect.Height + sourceRect.Y; h++)
                    {
                        var startPixelIndex = (int)h * width + (int)sourceRect.X;//每一行的起始像素位置

                        var startRealIndexPtr = (int)startPixelIndex * pixelByteLength;//起始像素在像素字节数组中的起始索引

                        var source = sp + startRealIndexPtr;
                        var dest = dp + startRealIndexPtr;
                        Buffer.MemoryCopy(source, dest, stride, stride);
                    }
                }
            }
        }
#pragma warning disable CS0414 // 字段“AvaloniaCefBrowser.count”已被赋值，但从未使用过它的值
        byte count = 0;
#pragma warning restore CS0414 // 字段“AvaloniaCefBrowser.count”已被赋值，但从未使用过它的值

        internal void OnPopupShow(bool show)
        {
            if (_popup == null)
            {
                return;
            }
            _popup.IsVisible = show;

            //Dispatcher.UIThread.InvokeAsync(() => _popup.IsOpen = show);
        }
        private CefRectangle CurrentPopupRectangle;
        internal void OnPopupSize(CefRectangle rectangle)
        {
            if (CurrentPopupRectangle.Height != rectangle.Height || CurrentPopupRectangle.Width != rectangle.Width || _popupImageBitmap != null)
            {
                CurrentPopupRectangle.Height = rectangle.Height;
                CurrentPopupRectangle.Width = rectangle.Width;
                _popupImageBitmap?.Dispose();
                _popupImageBitmap = new WriteableBitmap(new PixelSize((int)CurrentPopupRectangle.Width, (int)CurrentPopupRectangle.Height), new Vector(96, 96), PixelFormat.Bgra8888, AlphaFormat.Opaque);
                _popupImage.Source = this._popupImageBitmap;

                _popup.Width = CurrentPopupRectangle.Width;
                _popup.Height = CurrentPopupRectangle.Height;
                Point showPoint = new Point(rectangle.X - 2, rectangle.Y - 2);
                _popup.Margin = new Thickness(showPoint.X, showPoint.Y, 0, 0);
            }
        }
        /// <summary>
        /// 创建函数的注册信息
        /// </summary>
        /// <returns></returns>
        internal CefDictionaryValue CreateJavascriptRegisterInfo()
        {
            var alljsObject = JavascriptRepository.GetAll();
            CefDictionaryValue jsdict = CefDictionaryValue.Create();
            foreach (var jsRegObject in alljsObject)
            {
                CefDictionaryValue jsobject = CefDictionaryValue.Create();
                jsobject.SetInt("Id", jsRegObject.ObjectId);
                jsobject.SetString("Name", jsRegObject.Name);
                jsobject.SetString("ObjectTypeName", jsRegObject.ObjectTypeName);
                jsobject.SetString("AssemblyName", jsRegObject.AssemblyName);
                CefListValue methods = CefListValue.Create();
                for (int i = 0; i < jsRegObject.JavascriptMethods.Count; i++)
                {
                    methods.SetString(i, jsRegObject.JavascriptMethods[i].MethodName);
                }
                jsobject.SetList("Methods", methods);
                jsdict.SetDictionary(jsRegObject.Name, jsobject);
            }
            return jsdict;
        }

        /// <summary>
        /// 执行JavaScript代码
        /// </summary>
        /// <param name="jsCode"></param>
        public void ExecuteJavascript(string jsCode)
        {
            _browser?.GetMainFrame().ExecuteJavaScript(jsCode, null, 0);
        }

    }
}
