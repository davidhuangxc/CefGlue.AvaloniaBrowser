﻿using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Threading;
using CefGlue.Avalonia;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefGlue.AvaloniaTest
{
    public class InnerObject
    {
        public string Name { get; set; } = "";
        public int Value { get; set; }
    }

    public class MyInnerObject:InnerObject
    {
        public IList<string> DataList { get; set; }
    }
    public class BindingTestClass
    {
 
       
        public void SetAbc(int c = 0)
        {

        }

        public void S()
        {

        }
        public DateTime GetDate(String ac = "abcjkahkh")
        {
            return DateTime.Now;
        }

        public string GetString()
        {
            return "Hello World!";
        }

        public int GetInt()
        {
            return Random.Shared.Next();
        }

        public double GetDouble()
        {
            return Random.Shared.NextDouble()*double.MaxValue;
        }

        public bool GetBool()
        {
            return true;
        }



        public string[] GetList()
        {
            return new[] { "item 1", "item 2", "item 3" };
        }

        public IDictionary<string, object> GetDictionary()
        {
            string mainWindowTitle = null;
            if (Dispatcher.UIThread.CheckAccess())
            {
                mainWindowTitle= Application.Current.GetMainWindow().Title;
            }
            else
            {
                Dispatcher.UIThread.InvokeAsync(
                    () =>
                {
                    mainWindowTitle = Application.Current.GetMainWindow().Title;
                }).Wait();
            }
            return new Dictionary<string, object>
            {
                { "Name", "This is a dictionary" },
                { "Value", Random.Shared.NextSingle()*10000 },
                { "Random", Application.Current.GetMainWindow().TryGetPlatformHandle().Handle.ToInt64() },
                { "MainWindowName",mainWindowTitle  }
            };
        }

        public InnerObject GetObject()
        {
            
            return new InnerObject { Name = "This is an object", Value = 5 };
        }

        public InnerObject GetObjectWithParams(int anIntParam, string aStringParam, List<InnerObject>  anObjectParam, int[] intArrayParam)
        {
            return new InnerObject { Name = Newtonsoft.Json.JsonConvert.SerializeObject(new { AnIntParam= anIntParam, AStringParam= aStringParam, AnObjectParam=anObjectParam,IntArray= intArrayParam }), 
                Value = anObjectParam.Count+intArrayParam.Count() };
        }

        public async Task<string> AsyncGetObjectWithParams(string aStringParam)
        {
            //Console.WriteLine(DateTime.Now + ": Called " + nameof(AsyncGetObjectWithParams));
            await Task.Delay(50).ConfigureAwait(false);
            //Console.WriteLine(DateTime.Now + ":  Continuing " + nameof(AsyncGetObjectWithParams));
            return DateTime.Now.ToString() + "," + aStringParam;
        }

        public string[] GetObjectWithParamArray(int anIntParam, params string[] paramWithParamArray)
        {
            return paramWithParamArray;
        }

        public List<MyInnerObject> GetObjectArray()
        {
            List<MyInnerObject> dataList = new List<MyInnerObject>();
            for (int i = 0; i < Random.Shared.Next(10); i++)
            {
                MyInnerObject myInnerObject = new MyInnerObject()
                {
                    Name = $"{i},{DateTime.Now.Ticks}",
                    Value = Random.Shared.Next()
                };
                List<string> subDataList=new List<string>();
                for (int l = 0; l < Random.Shared.Next(15); l++)
                {
                    subDataList.Add($"l={l},value={Random.Shared.NextInt64()}");
                }
                myInnerObject.DataList = subDataList;
                dataList.Add(myInnerObject);
            }
            return dataList;
        }
    }
}
