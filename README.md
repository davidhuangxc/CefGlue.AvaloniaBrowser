# CefGlue.AvaloniaBrowser
1.使用Xilium.CefGlue做的一个基于avalonia框架的cef浏览器控件
2.使用的cef版本是102.0.10 。
3.可以在Windows和Linux系统（桌面环境）下面跑起来，这个版本在Windows x64环境和Linux x64环境可以运行起来，但在linux arm64环境下没能跑起来（感觉是cef库文件libcef.so的问题），但将cefglue这个C#工程的代码降级到cef的101.0.15版本就能在Linux arm64的桌面环境中运行起来，展示的程序运行窗口效果和Windows下一致，macos因为没有环境进行测试验证，所以macos环境时未经验证。
4.debug的时候，将下载的对应cef版本（102.0.10）Release目录下的所有文件复制到debug目录下，同时也需要把cef的Resource目录下的资源文件也复制到debug目录下。
5.提供了最基本的javascript调用C#类里面方法的实现。
6.处理了中文输入的问题，部分代码借助了cefnet的实现
7.项目是基于https://github.com/VitalElement/CefGlue.Core 改造而来
8.
程序的运行界面（Windows系统），Linux系统下能展示窗口界面，但不能显示调试工具界面
![image](https://user-images.githubusercontent.com/23723321/230533288-00b307ec-4b6b-484b-86c9-8ffccd49126a.png)

